"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showFilms = void 0;
function showFilms(filmsInfo) {
    var filmContainer = document.getElementById('film-container');
    filmContainer.innerHTML = '';
    for (var _i = 0, filmsInfo_1 = filmsInfo; _i < filmsInfo_1.length; _i++) {
        var film = filmsInfo_1[_i];
        filmContainer.innerHTML = "<div class=\"col-lg-3 col-md-4 col-12 p-2\">\n                            <div class=\"card shadow-sm\">\n                                <img\n                                    src=\"https://image.tmdb.org/t/p/original/" + film.posterPath + "\"\n                                />\n                                <svg\n                                    xmlns=\"http://www.w3.org/2000/svg\"\n                                    stroke=\"red\"\n                                    fill=\"red\"\n                                    width=\"50\"\n                                    height=\"50\"\n                                    class=\"bi bi-heart-fill position-absolute p-2\"\n                                    viewBox=\"0 -2 18 22\"\n                                >\n                                    <path\n                                        fill-rule=\"evenodd\"\n                                        d=\"M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z\"\n                                    />\n                                </svg>\n                                <div class=\"card-body\">\n                                    <p class=\"card-text truncate\">\n                                        " + film.overview + "\n                                    </p>\n                                    <div\n                                        class=\"\n                                            d-flex\n                                            justify-content-between\n                                            align-items-center\n                                        \"\n                                    >\n                                        <small class=\"text-muted\">" + film.releaseDate + "</small>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>";
    }
}
exports.showFilms = showFilms;
