import { searchByName } from './components/searchFilms/searchFilmsByName';
import { getPopularFilms } from './components/searchFilms/getPopularFilms';
import { getTopRatedMovies } from './components/searchFilms/getTopRatedMovies';
import { getUpcoming } from './components/searchFilms/getUpcoming';

export async function render(): Promise<void> {
    // TODO render your app here

    getPopularFilms();
    clickListener('submit', searchByName);
    clickListener('popular', getPopularFilms);
    clickListener('top_rated', getTopRatedMovies);
    clickListener('upcoming', getUpcoming);


    
    function clickListener(id: string, func: () => void) {
        const btn = document.getElementById(id);
        if (btn)
            btn.addEventListener('click', func);
    }
}