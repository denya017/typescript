import { IFilm } from '../models/IFilm';
import { fillLikeSVG, likeFilm } from '../likeFilm/likeFilm';

function showFilms(filmsInfo: Array<IFilm>): void {
    const filmContainer = <HTMLElement>document.getElementById('film-container');
    if (filmsInfo) {
        filmContainer.innerHTML = '';
    }

    for (const film of filmsInfo) {
        let srcImg:string;
        if(film.posterPath ==null) {
            srcImg = 'https://www.themoviedb.org/assets/2/apple-touch-icon-cfba7699efe7a742de25c28e08c38525f19381d31087c69e89d6bcb8e3c0ddfa.png';
        }
        else srcImg = 'https://image.tmdb.org/t/p/original/' + film.posterPath;
        filmContainer.innerHTML += `<div class='col-lg-3 col-md-4 col-12 p-2'>
                            <div class='card shadow-sm'>
                                <img
                                    src='${srcImg}'
                                 alt="${film.title}'s poster not found"/>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    stroke='red'
                                    fill='#ff000078'
                                    width='50'
                                    height='50'
                                    class='bi bi-heart-fill position-absolute p-2'
                                    id='${film.id}'
                                    viewBox='0 -2 18 22'
                                >
                                    <path
                                        fill-rule='evenodd'
                                        d='M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
                                    />
                                </svg>
                                <div class='card-body'>
                                    <p class='card-text truncate'>
                                        ${film.overview}
                                    </p>
                                    <div
                                        class='
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        '
                                    >
                                        <small class='text-muted'>${film.releaseDate}</small>
                                    </div>
                                </div>
                            </div>
                        </div>`;
        fillLikeSVG(film.id.toString());
    }

    const elements = document.getElementsByClassName("bi-heart-fill");
    for(let i = 0; i < elements.length ; i++) {
        elements[i].addEventListener('click', () => likeFilm(elements[i].id));
    }
}

export { showFilms };