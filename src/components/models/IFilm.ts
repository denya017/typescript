interface IFilm {
    id:number,
    title:string,
    posterPath: string,
    overview: string,
    releaseDate:string
}

export { IFilm }