import { API_key, path } from '../api-setting';
import { searchMovies } from './search';
import { showFilms } from '../showFilms/showFilms';


function getPopularFilms(): void {

    const url = `${path}/movie/popular?api_key=${API_key}`;
    searchMovies(url).then(resp => showFilms(resp));
}

export {
    getPopularFilms
};