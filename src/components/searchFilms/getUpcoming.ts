import { API_key, path } from '../api-setting';
import { searchMovies } from './search';
import { showFilms } from '../showFilms/showFilms';

function getUpcoming(): void {

    const url = `${path}/movie/upcoming?api_key=${API_key}`;
    searchMovies(url).then(resp => showFilms(resp));
}

export {
    getUpcoming
};