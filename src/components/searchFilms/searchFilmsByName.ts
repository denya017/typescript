import '../api-setting';
import { showFilms } from '../showFilms/showFilms';
import { searchMovies } from './search';
import { API_key, path } from '../api-setting';

function searchByName(): void {
    const nameMovie = (<HTMLInputElement>document.getElementById('search')).value.toString();
    if (nameMovie != '') {
        const url = `${path}/search/movie?api_key=${API_key}&query=${nameMovie}`
        searchMovies(url).then(resp => showFilms(resp));
    }
}

export {
    searchByName
};