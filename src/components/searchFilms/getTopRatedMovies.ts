import { API_key, path } from '../api-setting';
import { searchMovies } from './search';
import { showFilms } from '../showFilms/showFilms';

function getTopRatedMovies(): void {

    const url = `${path}/movie/top_rated?api_key=${API_key}`;
    searchMovies(url).then(resp => showFilms(resp));
}

export {
    getTopRatedMovies
};