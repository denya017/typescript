import { IFilm } from '../models/IFilm';

async function searchMovies(url:string): Promise<Array<IFilm>> {

    const filmArray: IFilm[] = [];
    const movies = [];
    const films = fetch(url)
        .then(
            successResponse => {
                if (successResponse.status != 200) {
                    console.log('!200');
                    return null;
                } else {
                    console.log('ok');
                    return successResponse.json();
                }
            },
            failResponse => {
                console.log('fail:' + failResponse);
                return null;
            },
        );
    movies.push(films);

    const results = await Promise.all(movies);

    for (const res of results[0].results) {
        filmArray.push({
            id: res.id,
            overview: res.overview,
            posterPath: res.poster_path,
            releaseDate: res.release_date,
            title: res.title,
        });
    }
    return filmArray;
}


export { searchMovies }