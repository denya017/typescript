function likeFilm(id: string): void {
    console.log(id);
    let idFilms: string[];
    if (!localStorage.getItem('idFavoriteFilms')) {
        idFilms = [id];
    } else {
        idFilms = JSON.parse(<string>localStorage.getItem('idFavoriteFilms'));
        const filmIndex = idFilms.indexOf(id);
        const svgElement: HTMLElement & SVGElement = document.getElementById(id) as HTMLElement & SVGElement;
        if (filmIndex == -1) {
            svgElement.setAttribute('fill', 'red');
            idFilms.push(id);
        } else {
            svgElement.setAttribute('fill', 'rgba(255,0,0,0.47)');
            idFilms.splice(filmIndex, 1);
        }
    }
    localStorage.setItem('idFavoriteFilms', JSON.stringify(idFilms));
}

function fillLikeSVG(id: string): void {
    if (localStorage.getItem('idFavoriteFilms')) {
        const idFilms: string[] = JSON.parse(<string>localStorage.getItem('idFavoriteFilms'));
        const filmIndex = idFilms.indexOf(id);
        const svgElement: HTMLElement & SVGElement = document.getElementById(id) as HTMLElement & SVGElement;
        if (filmIndex != -1) {
            svgElement.setAttribute('fill', 'red');
        }
    }
}

export { likeFilm, fillLikeSVG };